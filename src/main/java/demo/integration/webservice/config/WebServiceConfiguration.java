package demo.integration.webservice.config;

import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.ws.MarshallingWebServiceInboundGateway;
import org.springframework.messaging.MessageChannel;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.endpoint.adapter.MessageEndpointAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

@EnableWs
@Configuration
public class WebServiceConfiguration extends WsConfigurerAdapter {
	
	@Bean
	public MarshallingWebServiceInboundGateway ws() {
	    MarshallingWebServiceInboundGateway 
	    	entryPoint = new MarshallingWebServiceInboundGateway(jaxb2Marshaller());
	    entryPoint.setRequestChannel(inputDataChannel());
	    return entryPoint;
	}
	
	@Bean
	@Description("Message containing the input data from web service.")
	public MessageChannel inputDataChannel() {
	    return new DirectChannel();
	}
	
	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
	    Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
	    jaxb2Marshaller.setContextPath("demo.integration.pojos");
	    return jaxb2Marshaller;
	}
	
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
	    MessageDispatcherServlet servlet = new MessageDispatcherServlet();
	    servlet.setApplicationContext(context);
	    servlet.setTransformWsdlLocations(true);
	    return new ServletRegistrationBean(servlet, "/ws/*");
	}
	
	@Bean
	public MessageEndpointAdapter messageEndpointAdapter() {
		MessageEndpointAdapter adapter = new MessageEndpointAdapter();
	    return adapter;
	}
}
