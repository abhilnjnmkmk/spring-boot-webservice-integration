package demo.integration.webservice.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.ws.MarshallingWebServiceInboundGateway;
import org.springframework.ws.server.endpoint.mapping.UriEndpointMapping;

@Configuration
public class WebServiceMapping {
	@Autowired
	private MarshallingWebServiceInboundGateway entryPoint;

	@Bean
	public UriEndpointMapping uriEndpointMapping() {
	    UriEndpointMapping uriEndpointMapping = new UriEndpointMapping();
	    uriEndpointMapping.setDefaultEndpoint(entryPoint);
	    return uriEndpointMapping;
	}
}
