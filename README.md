# Spring SOAP Web Service with Spring Boot and Spring Integration without piece of XML configuration #

As many of you know, current future of Spring Framework is to create applications without any piece of XML configuration. Just with Java classes containing annotations. Because why? Because XML is an evil, unsecured...and...better no comment...:-\
From my point of view getting rid of XML will make some of the Spring Projects hardly usable, like Spring Security or Spring Batch, can you imagine to be configuring difficult Spring Batch job just with annotations?

But there are exceptions like creating Spring Web Service with Spring Boot and Spring Integration, where if you're familiar with architecture of SOAP Spring Web Services then it's fun, especially with Spring Integration behind your back...

## What do we need ##

* Configured **MessageDispatcher** servlet, because it does a lot of things for us. Like endpoint mapping.  Remember, it does not override default DispatcherServlet of Spring Boot. 

```
    @Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext context) {
	    MessageDispatcherServlet servlet = new MessageDispatcherServlet();
	    servlet.setApplicationContext(context);
	    servlet.setTransformWsdlLocations(true);
	    return new ServletRegistrationBean(servlet, "/ws/*");
	}
```

* We need to have our web service callable over MessageDispatcherServlet, so we need endpoint adapter which is used by MessageDispatcherServlet to invoke our Spring Integration infrastructure:

```
    @Bean
	public MessageEndpointAdapter messageEndpointAdapter() {
		MessageEndpointAdapter adapter = new MessageEndpointAdapter();
	    return adapter;
	}
```


* We want to use Spring Integration for processing the payload from Spring Web Service, so we need **MarshallingWebServiceInboundGateway**. For example:

```
    @Bean
	public MarshallingWebServiceInboundGateway ws() {
	    MarshallingWebServiceInboundGateway 
	    	entryPoint = new MarshallingWebServiceInboundGateway(jaxb2Marshaller());
	    entryPoint.setRequestChannel(inputDataChannel());
	    return entryPoint;
	}
	
	@Bean
	@Description("Message containing the input data from web service.")
	public MessageChannel inputDataChannel() {
	    return new DirectChannel();
	}
```
* Service Activator receiving input payload from previously mentioned Spring Web Service, in my case:
(notice the value of **inputChannel** attribute of **ServiceActivator annotation**)
```
@Configuration
@Import({WebServiceConfiguration.class, WebServiceMapping.class})
public class IntegrationInfrastructure {
    
    @ServiceActivator(inputChannel="inputDataChannel")
    public WeatherDataResponse weatherDataForCity(final WeatherDataRequest request) {
    	final WeatherDataResponse response = new WeatherDataResponse();
    	response.setCity(request.getCity());
    	response.setTemperature("34");
    	return response;
    }
}

```
Yes, I didn't complicate things up, every request for weather in the city in my demo will return asked city with 34 Celsius...add here your logic or whatever you want...

* And last thing, of course, as in every SOAP web service, XSD schema for request and reply of your web service:


```
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" 
		   xmlns:tns="http://spring.io/tomask79/spring-boot-ws-integration"
           targetNamespace="http://spring.io/tomask79/spring-boot-ws-integration" 
           elementFormDefault="qualified">
           
  <xs:element name="WeatherDataRequest">
    <xs:complexType>
      <xs:sequence>
        <xs:element type="xs:string" name="city"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
  <xs:element name="WeatherDataResponse">
    <xs:complexType>
      <xs:sequence>
        <xs:element type="xs:string" name="city"/>
        <xs:element type="xs:string" name="temperature"/>
      </xs:sequence>
    </xs:complexType>
  </xs:element>
  
</xs:schema>
```

Now simply start main DemoApplication class or launch compiled jar:

**java -jar path_to_compiled_project_jar**

and test Web Service with attached request.xml file via "curl" (if you're on Mac/Unix). Simply run:


```
curl --header "content-type: text/xml" -d @request http://localhost:8080/ws
```

If you configured and compiled correctly then you should get:


```
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
<SOAP-ENV:Header/>
<SOAP-ENV:Body>
<ns2:WeatherDataResponse 
       xmlns:ns2=
"http://spring.io/tomask79/spring-boot-ws-integration">
<ns2:city>Spain</ns2:city>
<ns2:temperature>34</ns2:temperature>
</ns2:WeatherDataResponse>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```

Congratulations, you just created Spring Web Service with Spring Boot and Spring Integration! Without any piece of XML...It's nice in this case, isn't it?